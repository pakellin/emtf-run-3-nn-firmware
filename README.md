This is the Vivado HLS project used to build the NN used in EMTF firmware. This allows us to keep using hls4ml, while using an hls 
wrapper around the NN to do pre-processing and post-processing while keeping the firmware interface the same.

To use:
vivado_hls -f build_prj.tcl

You can adjust the settings in the project.tcl file.

If you are importing a new neural network from hls4ml, you can just copy the firmware into the firmware folder,
but you will need to remove the INTERFACE and (optionally) PIPELINE #pragmas in the top NN module.

If the nn name changes, add the new one into the project.tcl file.
