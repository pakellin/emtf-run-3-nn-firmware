#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include "firmware/emtfnn_wrapper.h"
#include "firmware/nnet_utils/nnet_helpers.h"

// hls-fpga-machine-learning insert bram

#define CHECKPOINT 5000

// File must match this
#define N_WRAPPER_INPUTS 41
#define N_WRAPPER_OUTPUTS 3

namespace nnet {
bool trace_enabled = true;
std::map<std::string, void *> *trace_outputs = NULL;
size_t trace_type_size = sizeof(double);
} // namespace nnet

int main(int argc, char **argv) {
    // load input data from text file
    std::ifstream fin("tb_data/tb_input_features.dat");
    // load predictions from text file
    std::ifstream fpr("tb_data/tb_output_predictions.dat");

#ifdef RTL_SIM
    std::string RESULTS_LOG = "tb_data/rtl_cosim_results.log";
#else
    std::string RESULTS_LOG = "tb_data/csim_results.log";
#endif
    std::ofstream fout(RESULTS_LOG);

    std::string iline;
    std::string pline;
    int e = 0;

    if (fin.is_open() && fpr.is_open()) {
        int num_of_vals[13] = {0};
        while (std::getline(fin, iline) && std::getline(fpr, pline)) {
            if (e % CHECKPOINT == 0)
                std::cout << "\nProcessing input " << e << std::endl;
            char *cstr = const_cast<char *>(iline.c_str());
            char *current;
            std::vector<float> in;
            current = strtok(cstr, " ");
            while (current != NULL) {
                in.push_back(atof(current));
                current = strtok(NULL, " ");
            }
            cstr = const_cast<char *>(pline.c_str());
            std::vector<float> pr;
            current = strtok(cstr, " ");
            while (current != NULL) {
                pr.push_back(atof(current));
                current = strtok(NULL, " ");
            }

            // Function inputs and Outputs
            unsigned int input[N_WRAPPER_INPUTS];
            nnet::copy_data<float, unsigned int, 0, N_WRAPPER_INPUTS>(in, input);
            ap_uint<8> pT;
            ap_uint<7> dxy;
            ap_uint<1> valid;

            ap_uint<4> ch_id[5];
            ch_id[0] = input[32] + 1;
            ch_id[1] = input[32] + 1;
            ch_id[2] = input[33] + 1;
            ch_id[3] = input[34] + 1;
            ch_id[4] = input[35] + 1;

            for(int i=0; i<5; i++)
                if(ch_id[i] > 12)
                    ch_id[i] = 0;

            ap_uint<1> me11 = input[36];

            if(me11){
                std::cout << "Ch_id: " << input[32]+1 << "\n";
                num_of_vals[input[32]] = num_of_vals[input[32]] + 1;
            }


            ap_uint<3> sector = input[38];
            ap_uint<7> theta = input[39];
            ap_uint<4> mode = input[40];

            ap_uint<2> st1a_segs_valid = 1;
            ap_uint<2> st1b_segs_valid = 0;

            // Call top level function
            emtfnn_wrapper(
                        input[0], input[1], input[2], input[3], input[4], input[5],  // dphi
                        input[6], input[7], input[8], input[9], input[10], input[11],  // dphi signs
                        input[12], input[13], input[14], input[15], input[16], input[17], // dtheta
                        input[18], input[19], input[20], input[21], input[22], input[23], // dtheta signs
                        input[24], input[25], input[26], input[27], // cpattern
                        theta, // theta
                        mode, // mode
                        sector, // sector
                        ch_id, // ch_id 
                        st1a_segs_valid, // me11a_segs_valid
                        st1b_segs_valid, // me11b_segs_valid
                        pT, dxy, valid
            );

            if (e % CHECKPOINT == 0) {
                std::cout << "Predictions" << std::endl;
                // Print correct outputs
                for(int i = 0; i < N_WRAPPER_OUTPUTS; i++) {
                  std::cout << pr[i] << " ";
                }
                std::cout << std::endl;
                std::cout << "Quantized predictions" << std::endl;
                // Print hls outputs
                ap_uint<8> results[N_WRAPPER_OUTPUTS] = {pT, ap_uint<8>(dxy), ap_uint<8>(valid)};
                nnet::print_result<ap_uint<8>, N_WRAPPER_OUTPUTS>(results, std::cout, true);
            }
            e++;

            // Validate
            /*
            int threshold = 0.1;
            if( (pr[0] + threshold < pT) or (pr[0] - threshold > pT)){
            	std::cout << "Found pT difference larger than threshold: " << pr[0] << " vs " << pT << "\n";
            }
            if( (pr[1] + threshold < dxy) or (pr[1] - threshold > dxy)){
            	std::cout << "Found dxy difference larger than threshold: " << pr[1] << " vs " << dxy << "\n";
            }
            */

            // Write outputs to file
            ap_uint<8> results[N_WRAPPER_OUTPUTS] = {pT, ap_uint<8>(dxy), ap_uint<8>(valid)};
            nnet::print_result<ap_uint<8>, N_WRAPPER_OUTPUTS>(results, fout);
        }
        for(int i=0; i<13; i++)
            std::cout << num_of_vals[i] << ", ";
        std::cout << "\n";

        fin.close();
        fpr.close();
    } else {
        std::cout << "INFO: Unable to open input/predictions file, not testing hls." << std::endl;
    }

    fout.close();
    std::cout << "INFO: Saved inference results to file: " << RESULTS_LOG << std::endl;

    return 0;
}
