#ifndef DEFINES_H_
#define DEFINES_H_

#include "ap_fixed.h"
#include "ap_int.h"
#include "nnet_utils/nnet_types.h"
#include <cstddef>
#include <cstdio>

// hls-fpga-machine-learning insert numbers
#define N_INPUT_1_1 34
#define N_LAYER_2 24
#define N_LAYER_2 24
#define N_LAYER_5 20
#define N_LAYER_5 20
#define N_LAYER_8 16
#define N_LAYER_8 16
#define N_LAYER_11 2
#define N_LAYER_11 2
#define N_LAYER_11 2

// hls-fpga-machine-learning insert layer-precision
typedef ap_uint<13> input_t;
typedef ap_fixed<17,7> dense_accum_t;
typedef ap_fixed<16,7> layer2_t;
typedef ap_fixed<20,8> dense_weight_t;
typedef ap_fixed<20,8> dense_bias_t;
typedef ap_uint<1> layer2_index;
typedef ap_fixed<16,7> layer4_t;
typedef ap_fixed<18,8> activation_table_t;
typedef ap_fixed<16,7> dense_1_accum_t;
typedef ap_fixed<15,7> layer5_t;
typedef ap_fixed<20,8> dense_1_weight_t;
typedef ap_fixed<20,8> dense_1_bias_t;
typedef ap_uint<1> layer5_index;
typedef ap_fixed<15,7> layer7_t;
typedef ap_fixed<18,8> activation_1_table_t;
typedef ap_fixed<15,9> dense_2_accum_t;
typedef ap_fixed<14,9> layer8_t;
typedef ap_fixed<20,8> dense_2_weight_t;
typedef ap_fixed<20,8> dense_2_bias_t;
typedef ap_uint<1> layer8_index;
typedef ap_fixed<14,9> layer10_t;
typedef ap_fixed<18,8> activation_2_table_t;
typedef ap_fixed<14,12> dense_3_accum_t;
typedef ap_fixed<10,10,AP_TRN,AP_SAT> layer11_t;
typedef ap_fixed<20,8> dense_3_weight_t;
typedef ap_uint<1> bias11_t;
typedef ap_uint<1> layer11_index;
typedef ap_fixed<10,10> layer12_t;
typedef ap_fixed<18,8> dense_3_linear_table_t;
typedef ap_uint<8> result_t;

#endif
