#include <iostream>

#include "emtfnn.h"
#include "parameters.h"

void emtfnn(
    input_t inputs[N_INPUT_1_1],
    result_t layer13_out[N_LAYER_11]
) {

    // hls-fpga-machine-learning insert IO
    #pragma HLS INLINE 

#ifndef __SYNTHESIS__
    static bool loaded_weights = false;
    if (!loaded_weights) {
        // hls-fpga-machine-learning insert load weights
        nnet::load_weights_from_txt<dense_weight_t, 816>(w2, "w2.txt");
        nnet::load_weights_from_txt<dense_bias_t, 24>(b2, "b2.txt");
        nnet::load_weights_from_txt<dense_1_weight_t, 480>(w5, "w5.txt");
        nnet::load_weights_from_txt<dense_1_bias_t, 20>(b5, "b5.txt");
        nnet::load_weights_from_txt<dense_2_weight_t, 320>(w8, "w8.txt");
        nnet::load_weights_from_txt<dense_2_bias_t, 16>(b8, "b8.txt");
        nnet::load_weights_from_txt<dense_3_weight_t, 32>(w11, "w11.txt");
        nnet::load_weights_from_txt<bias11_t, 2>(b11, "b11.txt");
        loaded_weights = true;
    }
#endif

    // ****************************************
    // NETWORK INSTANTIATION
    // ****************************************

    // hls-fpga-machine-learning insert layers

    layer2_t layer2_out[N_LAYER_2];
    #pragma HLS ARRAY_PARTITION variable=layer2_out complete dim=0
    nnet::dense<input_t, layer2_t, config2>(inputs, layer2_out, w2, b2); // dense

    layer4_t layer4_out[N_LAYER_2];
    #pragma HLS ARRAY_PARTITION variable=layer4_out complete dim=0
    nnet::relu<layer2_t, layer4_t, relu_config4>(layer2_out, layer4_out); // activation

    layer5_t layer5_out[N_LAYER_5];
    #pragma HLS ARRAY_PARTITION variable=layer5_out complete dim=0
    nnet::dense<layer4_t, layer5_t, config5>(layer4_out, layer5_out, w5, b5); // dense_1

    layer7_t layer7_out[N_LAYER_5];
    #pragma HLS ARRAY_PARTITION variable=layer7_out complete dim=0
    nnet::relu<layer5_t, layer7_t, relu_config7>(layer5_out, layer7_out); // activation_1

    layer8_t layer8_out[N_LAYER_8];
    #pragma HLS ARRAY_PARTITION variable=layer8_out complete dim=0
    nnet::dense<layer7_t, layer8_t, config8>(layer7_out, layer8_out, w8, b8); // dense_2

    layer10_t layer10_out[N_LAYER_8];
    #pragma HLS ARRAY_PARTITION variable=layer10_out complete dim=0
    nnet::relu<layer8_t, layer10_t, relu_config10>(layer8_out, layer10_out); // activation_2

    layer11_t layer11_out[N_LAYER_11];
    #pragma HLS ARRAY_PARTITION variable=layer11_out complete dim=0
    nnet::dense<layer10_t, layer11_t, config11>(layer10_out, layer11_out, w11, b11); // dense_3

    layer12_t layer12_out[N_LAYER_11];
    #pragma HLS ARRAY_PARTITION variable=layer12_out complete dim=0
    nnet::linear<layer11_t, layer12_t, linear_config12>(layer11_out, layer12_out); // dense_3_linear

    nnet::hybrid_activation<layer12_t, result_t, config13>(layer12_out, layer13_out); // hybrid_activation

}
