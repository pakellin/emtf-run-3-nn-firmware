#include <iostream>

#include "emtfnn_wrapper.h"
#include "emtfnn.h"

void emtfnn_wrapper(
    ap_uint<13> delta_phi0,
    ap_uint<13> delta_phi1,
    ap_uint<13> delta_phi2,
    ap_uint<13> delta_phi3,
    ap_uint<13> delta_phi4,
    ap_uint<13> delta_phi5,
    
    ap_uint<1> sign_phi0,
    ap_uint<1> sign_phi1,
    ap_uint<1> sign_phi2,
    ap_uint<1> sign_phi3,
    ap_uint<1> sign_phi4,
    ap_uint<1> sign_phi5,

    ap_uint<7> delta_th0,
    ap_uint<7> delta_th1,
    ap_uint<7> delta_th2,
    ap_uint<7> delta_th3,
    ap_uint<7> delta_th4,
    ap_uint<7> delta_th5,

    ap_uint<1> sign_th0,
    ap_uint<1> sign_th1,
    ap_uint<1> sign_th2,
    ap_uint<1> sign_th3,
    ap_uint<1> sign_th4,
    ap_uint<1> sign_th5,

	ap_uint<4> cpattern_0,
    ap_uint<4> cpattern_1,
    ap_uint<4> cpattern_2,
    ap_uint<4> cpattern_3,

	ap_uint<7> theta,

    ap_uint<4> mode,
    ap_uint<3> sector,
    ap_uint<4> ch_id[5],
    ap_uint<2> st1a_segs_valid,
    ap_uint<2> st1b_segs_valid,

    // Outputs
    ap_uint<8> &pT,
    ap_uint<7> &dxy,
    ap_uint<1> &valid_track
) {

    // Top level function pragmas
    #pragma HLS INTERFACE ap_ctrl_none port=return
    #pragma HLS INTERFACE ap_none port=pT,dxy,valid_track
    #pragma HLS ARRAY_PARTITION variable=ch_id complete dim=0
    #pragma HLS PIPELINE II=1
    #pragma HLS latency min=11 max=11

    input_t nn_input[34];
    result_t nn_output[2];

    // hls-fpga-machine-learning insert IO
    #pragma HLS ARRAY_PARTITION variable=nn_input complete dim=0
    #pragma HLS ARRAY_PARTITION variable=nn_output complete dim=0

    // Get valid bits for Station 1 and me11
    ap_uint<1> st1a_v = (st1a_segs_valid != 0);
    ap_uint<1> st1b_v = (st1b_segs_valid != 0);
    ap_uint<1> me11_st1a_v = (st1a_v && ((ch_id[0] >= 1 && ch_id[0] <= 3) || ch_id[0] == 10)); // ME11 station 1a if ch is 1,2,3,10
    ap_uint<1> me11_st1b_v = (st1b_v && (ch_id[1] >= 1 && ch_id[1] <= 3)); // ME11 station 1b if CH: 1,2,3
    ap_uint<1> me11_v = (me11_st1a_v | me11_st1b_v);

    // Set FR Bits -- need sector[0], station, and chamber id
    ap_uint<4> fr_bits;
    for(int st=0; st<4; st++){
        #pragma HLS UNROLL
        // Get FR bits for Stations 1b, 2, 3, and 4
        fr_bits[st] = fr_LUT[sector[0]][st+1][ch_id[st+1]];

        // If station 1 is 1a -> use station 0 (1a) fr_bits
        if(st == 0 && st1a_v == 1)
            fr_bits[st] = fr_LUT[sector[0]][0][ch_id[0]];
    }

    // Output is valid if the track has more than 1 station
    valid_track = 1;
    if(mode == 0 or mode == 1 or mode == 2 or mode == 4 or mode == 8){
        valid_track = 0;
    }

    // Clean the tracks
    if(mode[3] == 0){
        // No ME1
        cpattern_0 = 0;
        fr_bits[0] = 0;
        delta_phi0 = 0;
        delta_phi1 = 0;
        delta_phi2 = 0;
        delta_th0 = 0;
        delta_th1 = 0;
        delta_th2 = 0;
        me11_v = 0;
    }

    if(mode[2] == 0){
        // No ME2
        cpattern_1 = 0;
        fr_bits[1] = 0;
        delta_phi0 = 0;
        delta_phi3 = 0;
        delta_phi4 = 0;
        delta_th0 = 0;
        delta_th3 = 0;
        delta_th4 = 0;
    }

    if(mode[1] == 0){
        // No ME3
        cpattern_2 = 0;
        fr_bits[2] = 0;
        delta_phi1 = 0;
        delta_phi3 = 0;
        delta_phi5 = 0;
        delta_th1 = 0;
        delta_th3 = 0;
        delta_th5 = 0;
    }

    if(mode[0] == 0){
        // No ME4
        cpattern_3 = 0;
        fr_bits[3] = 0;
        delta_phi2 = 0;
        delta_phi4 = 0;
        delta_phi5 = 0;
        delta_th2 = 0;
        delta_th4 = 0;
        delta_th5 = 0;
    }

    
    // Fill input Array - zeros are placed in front
    nn_input[0] = delta_phi0;
    nn_input[1] = delta_phi1;
    nn_input[2] = delta_phi2;
    nn_input[3] = delta_phi3;
    nn_input[4] = delta_phi4;
    nn_input[5] = delta_phi5;

    nn_input[6] = sign_phi0;
    nn_input[7] = sign_phi1;
    nn_input[8] = sign_phi2;
    nn_input[9] = sign_phi3;
    nn_input[10] = sign_phi4;
    nn_input[11] = sign_phi5;

    nn_input[12] = delta_th0;
    nn_input[13] = delta_th1;
    nn_input[14] = delta_th2;
    nn_input[15] = delta_th3;
    nn_input[16] = delta_th4;
    nn_input[17] = delta_th5;

    nn_input[18] = sign_th0;
    nn_input[19] = sign_th1;
    nn_input[20] = sign_th2;
    nn_input[21] = sign_th3;
    nn_input[22] = sign_th4;
    nn_input[23] = sign_th5;

    nn_input[24] = cpattern_0;
    nn_input[25] = cpattern_1;
    nn_input[26] = cpattern_2;
    nn_input[27] = cpattern_3;

    nn_input[28] = fr_bits[0];
    nn_input[29] = fr_bits[1];
    nn_input[30] = fr_bits[2];
    nn_input[31] = fr_bits[3];

    nn_input[32] = me11_v;
    nn_input[33] = theta;

    // Run the NN
    emtfnn(nn_input, nn_output);

    // Convert outputs to correct values
    pT = nn_output[0];
    dxy = static_cast<ap_uint<7>>(nn_output[1]);
}
