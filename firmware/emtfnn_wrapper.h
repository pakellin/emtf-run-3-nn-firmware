#ifndef EMTFNN_WRAPPER_H_
#define EMTFNN_WRAPPER_H_

#include "ap_fixed.h"
#include "ap_int.h"
#include "hls_stream.h"
#include "defines.h"
#include "emtfnn.h"

// Top Level function
void emtfnn_wrapper(
    ap_uint<13> delta_phi0,
    ap_uint<13> delta_phi1,
    ap_uint<13> delta_phi2,
    ap_uint<13> delta_phi3,
    ap_uint<13> delta_phi4,
    ap_uint<13> delta_phi5,
    
    ap_uint<1> sign_phi0,
    ap_uint<1> sign_phi1,
    ap_uint<1> sign_phi2,
    ap_uint<1> sign_phi3,
    ap_uint<1> sign_phi4,
    ap_uint<1> sign_phi5,

    ap_uint<7> delta_th0,
    ap_uint<7> delta_th1,
    ap_uint<7> delta_th2,
    ap_uint<7> delta_th3,
    ap_uint<7> delta_th4,
    ap_uint<7> delta_th5,

    ap_uint<1> sign_th0,
    ap_uint<1> sign_th1,
    ap_uint<1> sign_th2,
    ap_uint<1> sign_th3,
    ap_uint<1> sign_th4,
    ap_uint<1> sign_th5,

    ap_uint<4> cpattern_0,
    ap_uint<4> cpattern_1,
    ap_uint<4> cpattern_2,
    ap_uint<4> cpattern_3,

    ap_uint<7> theta,

    ap_uint<4> mode,
    ap_uint<3> sector,
    ap_uint<4> ch_id[5],
    ap_uint<2> st1a_segs_valid,
    ap_uint<2> st1b_segs_valid,

    ap_uint<8> &pT,
    ap_uint<7> &dxy,
    ap_uint<1> &valid_track
);



const unsigned int num_stations = 5;

// Taken from firmware, ptLUT_Address file
const ap_uint<13> fr_LUT[2][num_stations] = { 
    // sector[bit 0] == 0
    {
        "0b0110000100100",
        "0b0000001011010",
        "0b0101010101010",
        "0b0010101010100",
        "0b0010101010100"
    },
    // sector[bit 0] == 1
    {
        "0b0110000100100",
        "0b0000001011010",
        "0b0111010100100",
        "0b0000101011010",
        "0b0000101011010"
    }
};  


#endif
