//Numpy array shape [2]
//Min 0.000000000000
//Max 0.000000000000
//Number of zeros 2

#ifndef B14_H_
#define B14_H_

#ifndef __SYNTHESIS__
bias14_t b14[2];
#else
bias14_t b14[2] = {0, 0};
#endif

#endif
