variable project_name
set project_name "emtfnn_wrapper"
set nn_name "emtfnn"
variable backend
set backend "vivado"
variable part
set part "xc7vx690tffg1927-2"
variable clock_period
set clock_period 6.5 
#set clock_period 8
variable clock_uncertainty
set clock_uncertainty 5%
#set clock_uncertainty 8%
